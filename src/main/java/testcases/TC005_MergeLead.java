package testcases;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.LoginPage;
import wdMethods.ProjectMethods;
public class TC005_MergeLead extends ProjectMethods {

	@BeforeTest
	public void setData() {
		testCaseName="TC005_MergeLead";
		testDescription="Merging two leads";
		authors="Aditya";
		category="reg";
		testNodes ="Leads";
		dataSheetName="TC005";
	}
	@Test(dataProvider="fetchData")
	public void mergeLead(String userName, String password,String firstName) {
		new LoginPage()
		.typeUsername(userName)
		.typePassword(password)
		.clickLogin()
		.clickCRMSFA()
		.clickLead()
		.clickMergeLeads()
		.clickFromLeadIcon()
		.typeFirstName(firstName)
		.clickbuttonFindLead()
		.storeFirstLead()
		.clickFirstLeadId()
		.clickToLeadIcon()
		.typeFirstName(firstName)
		.clickbuttonFindLead()
		.clickSecondLeadId()
		.clickMergeButton()
		.clickFindLeadLink()
		.typeLeadId()
		.clickFindLeadButton()
		.verifyEmptyRecord();
	}
}
