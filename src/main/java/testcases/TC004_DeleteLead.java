package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC004_DeleteLead extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName="TC004_DeleteLead";
		testDescription="Delete existing lead";
		authors="Aditya";
		category="reg";
		testNodes ="Leads";
		dataSheetName="TC004";
	}
	
	@Test(dataProvider="fetchData")
	public void deleteLead(String username, String password, String phoneNumber) {
		new LoginPage()
		.typeUsername(username)
		.typePassword(password)
		.clickLogin()
		.clickCRMSFA()
		.clickLead()
		.clickFindLeadLink()
		.clickPhoneTab()
		.typePhoneNumber(phoneNumber)
		.clickFindLeadButton()
		.storeFirstLead()
		.clickFirstLeadId()
		.clickDeleteButton()
		.clickFindLeadLink()
		.typeLeadId()
		.clickFindLeadButton()
		.verifyEmptyRecord();
	}
}
