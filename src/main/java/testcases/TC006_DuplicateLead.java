package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC006_DuplicateLead extends ProjectMethods{
	
	@BeforeTest
	public void setData() {
		testCaseName="TC006_DuplicateLead";
		testDescription="Duplicate an already created lead";
		authors="Aditya";
		category="reg";
		testNodes ="Leads";
		dataSheetName="TC006";
	}

	@Test(dataProvider="fetchData")
	public void deleteLead(String username, String password, String emailAddress) {
		new LoginPage()
		.typeUsername(username)
		.typePassword(password)
		.clickLogin()
		.clickCRMSFA()
		.clickLead()
		.clickFindLeadLink()
		.clickEmailTab()
		.typeEmailAddress(emailAddress)
		.clickFindLeadButton()
		.storeFirstName()
		.clickFirstLeadName()
		.clickDuplicateLeadButton()
		.verifyDuplicateLeadTitle()
		.clickCreateLeadButton()
		.verifyFirstName(storedLead);
	}
}
