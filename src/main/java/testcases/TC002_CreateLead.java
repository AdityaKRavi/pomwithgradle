package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC002_CreateLead extends ProjectMethods{
	
	@BeforeTest
	public void setData() {
		testCaseName="TC002_CreateLead";
		testDescription="Create New Lead";
		authors="Aditya";
		category="reg";
		testNodes ="Leads";
		dataSheetName="TC002";
	}
	
	@Test(dataProvider = "fetchData")
	public void createLead(String uname, String pwd,String companyName, String firstName, 
			String lastName, String dataSource, String market_CampId, String title, String dept_Name,
			String revenue, String currency, String industryId, String ownershipId, String sicCode,   
			String tickerSymbol, String phoneAreaCode, String phoneNumber, String emailId, String webUrl, 
			String address1, String address2, String city, String state, String zipCode, String country) {
		
		new LoginPage()
		.typeUsername(uname)
		.typePassword(pwd)
		.clickLogin()
		.clickCRMSFA()
		.clickLead()
		.clickCreateLeadLink()
		.typeCompanyName(companyName)
		.typeFirstName(firstName)
		.typeLastName(lastName)
		.selectDataSource(dataSource)
		.selectMarketingCampaignId(market_CampId)
		.typeTitle(title)
		.typeDepartmentName(dept_Name)
		.typeAnnualRevenue(revenue)
		.selectCurrency(currency)
		.selectIndustryId(industryId)
		.selectOwnershipId(ownershipId)
		.typeSicCode(sicCode)
		.typeTickerSymbol(tickerSymbol)
		.typePhoneAreaCode(phoneAreaCode)
		.typePhoneNumber(phoneNumber)
		.typeEmailAddress(emailId)
		.typeWebUrl(webUrl)
		.typeAddress1(address1)
		.typeAddress2(address2)
		.typeCity(city)
		.selectCountry(country)
		.selectState(state)
		.typeZipCode(zipCode)
		.clickCreateLeadButton();
//		new ViewCreatedLeadPage().verifyFirstName(firstName);
	}
}
