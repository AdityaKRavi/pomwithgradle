package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC003_EditLead extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName="TC003_EditLead";
		testDescription="Edit already created Lead";
		authors="Aditya";
		category="reg";
		testNodes ="Leads";
		dataSheetName="TC003";
	}
	
	@Test(dataProvider="fetchData")
	public void editLead(String username, String password, String firstName, String companyName) {
		new LoginPage()
		.typeUsername(username)
		.typePassword(password)
		.clickLogin()
		.clickCRMSFA()
		.clickLead()
		.clickFindLeadLink()
		.typeFirstName(firstName)
		.clickFindLeadButton()
		.storeFirstLead()
		.clickFirstLeadId()
		.clickEditButton()
		.editCompanyName(companyName)
		.clickUpdateButton()
		.verifyUpdatedCompanyName(companyName);
	}
}
