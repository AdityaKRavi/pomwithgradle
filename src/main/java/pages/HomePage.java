package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class HomePage extends ProjectMethods {

	public HomePage() {
		PageFactory.initElements(driver, this);
	}
	
	@Then ("verify login is success")
	public HomePage verifyLogin() {
		System.out.println("You've successfully logged in");
		return new HomePage();
	}
	
	@FindBy(className="decorativeSubmit")
	private WebElement eleLogout;
	public LoginPage clickLogout() {
		click(eleLogout);
		return new LoginPage();
	}
	
	@FindBy(linkText="CRM/SFA")
	private WebElement crmsfaLink;
	@When("click on crmsfa")
	public MainPage clickCRMSFA() {
		click(crmsfaLink);
		return new MainPage();
	}
}





