package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeadPage extends ProjectMethods{

	public FindLeadPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath="(//input[@name='firstName'])[3]")
	private WebElement firstName;
	public FindLeadPage typeFirstName(String data) {
		type(firstName, data);
		return this;
	}
	@FindBy(xpath="//button[text()='Find Leads']")
	private WebElement findLeadButton;
	public FindLeadPage clickFindLeadButton() {
		click(findLeadButton);
		return this;
	}

	@FindBy(xpath="(//table[@class='x-grid3-row-table']//td[1])[1]//a")
	private WebElement firstLeadId;
	public FindLeadPage storeFirstLead() {
		storedLead = getText(firstLeadId);
		return this;
	}

	public ViewLeadPage clickFirstLeadId() {
		click(firstLeadId);
		return new ViewLeadPage();
	}

	@FindBy(xpath="//span[text( )='Phone']")
	private WebElement phoneTab;
	public FindLeadPage clickPhoneTab(){
		click(phoneTab);
		return this;
	}

	@FindBy(name="phoneNumber")
	private WebElement phoneNumber;
	public FindLeadPage typePhoneNumber(String data) {
		type(phoneNumber, data);
		return this;
	}
	
	@FindBy(className="x-paging-info")
	private WebElement emptyRecord;
	public FindLeadPage verifyEmptyRecord() {
		verifyExactText(emptyRecord, "No records to display");
		return this;
	}
	
	@FindBy(name="id")
	private WebElement leadId;
	public FindLeadPage typeLeadId() {
		type(leadId, storedLead);
		return this;
	}
	
	@FindBy(xpath = "//span[text( )='Email']")
	private WebElement emailTab;
	public FindLeadPage clickEmailTab() {
		click(emailTab);
		return this;
	}
	
	@FindBy(name="emailAddress")
	private WebElement emailAddress;
	public FindLeadPage typeEmailAddress(String data) {
		type(emailAddress, data);
		return this;
	}
	
	@FindBy(xpath="(//table[@class='x-grid3-row-table']//tr/td[3]//a)[1]")
	private WebElement firstLeadName;
	public FindLeadPage storeFirstName() {
		storedLead = getText(firstLeadName);
		return this;
	}
	
	public ViewLeadPage clickFirstLeadName() {
		click(firstLeadName);
		return new ViewLeadPage();
	}
}
