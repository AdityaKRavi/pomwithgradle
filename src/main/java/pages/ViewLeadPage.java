package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Then;
import wdMethods.ProjectMethods;

public class ViewLeadPage extends ProjectMethods{

	public ViewLeadPage() {
		PageFactory.initElements(driver, this);
	}
	
	@Then ("verify create lead is successfull")
	public ViewLeadPage verifyCreateLead() {
		System.out.println("The lead is created");
		return this;
	}
	
	@FindBy(id="viewLead_firstName_sp")
	private WebElement viewFirstName;
	public ViewLeadPage verifyFirstName(String data) {
		verifyExactText(viewFirstName, data);
		return this;
	}

	public ViewLeadPage verifyViewLeadTitlePage() {
		verifyTitle("View Lead | opentaps CRM");
		return this;
	}

	@FindBy(linkText="Edit")
	private WebElement editButton;
	public EditLeadPage clickEditButton() {
		click(editButton);
		return new EditLeadPage();
	}

	@FindBy(id="viewLead_companyName_sp")
	private WebElement updatedCompanyName;
	public ViewLeadPage verifyUpdatedCompanyName(String data) {
		verifyPartialText(updatedCompanyName, data);
		return this;
	}

	@FindBy(linkText="Delete")
	private WebElement deleteButton;
	public LeadsMainPage clickDeleteButton() {
		click(deleteButton);
		return new LeadsMainPage();
	}

	@FindBy(linkText="Find Leads")
	private WebElement findLeads;
	public FindLeadPage clickFindLeadLink() {
		click(findLeads);
		return new FindLeadPage();
	}

	@FindBy(linkText="Duplicate Lead")
	private WebElement duplicateLeadButton;
	public DuplicateLeadPage clickDuplicateLeadButton() {
		click(duplicateLeadButton);
		return new DuplicateLeadPage();
	}
}
