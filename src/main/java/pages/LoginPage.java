package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class LoginPage extends ProjectMethods {

	public LoginPage() {
		PageFactory.initElements(driver, this);
	}
	
	@Given ("I am on the Login Page")
	public LoginPage verifyLoginPage() {
		if (driver.getCurrentUrl().contains("http://leaftaps.com/opentaps/control/main"))
			System.out.println("I am on Login Page");
		return this;
	}
	@FindBy(id="username")
	private WebElement eleUsername;
	@When ("Enter username (.*)")
	public LoginPage typeUsername(String data) {
		type(eleUsername, data);
		return this;
	}
	@FindBy(id="password")
	private WebElement elePassword;
	@And ("Enter password (.*)")
	public LoginPage typePassword(String data) {
		type(elePassword, data);
		return this;
	}
	@FindBy(className ="decorativeSubmit")
	private WebElement eleLogin;
	@When ("click on login button")
	public HomePage clickLogin() {
		click(eleLogin);
		return new HomePage();
	}
}





