package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class ToMergeFindLead extends ProjectMethods {

	public ToMergeFindLead() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "//input[@name='firstName']")
	private WebElement firstName;
	public ToMergeFindLead typeFirstName(String data) {
		type(firstName, data);
		return this;
	}
	
	@FindBy(xpath = "//button[text()='Find Leads']")
	private WebElement buttonFindLead;
	public ToMergeFindLead clickbuttonFindLead() {
		click(buttonFindLead);
		return this;
	}
	
	@FindBy(xpath="(//div[@class='x-grid3-scroller']//table)[2]//a")
	private WebElement leadId;
	public MergeLeadsPage clickSecondLeadId() {
		click(leadId);
		switchToWindow(0);
		return new MergeLeadsPage();
	}
}
