package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class MainPage extends ProjectMethods {
	public MainPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(linkText="Leads")
	private WebElement leadLink;
	@When("I click Leads tab")
	public LeadsMainPage clickLead() {
		click(leadLink);
		return new LeadsMainPage();
	}
}
