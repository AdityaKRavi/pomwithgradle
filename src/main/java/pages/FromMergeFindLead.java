package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FromMergeFindLead extends ProjectMethods{
	
	public FromMergeFindLead() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "//input[@name='firstName']")
	private WebElement firstName;
	public FromMergeFindLead typeFirstName(String data) {
		type(firstName, data);
		return this;
	}
	
	@FindBy(xpath = "//button[text()='Find Leads']")
	private WebElement buttonFindLead;
	public FromMergeFindLead clickbuttonFindLead() {
		click(buttonFindLead);
		return this;
	}
	
	@FindBy(xpath="(//div[@class='x-grid3-scroller']//a)[1]")
	private WebElement leadId;
	public FromMergeFindLead storeFirstLead() {
		storedLead = getText(leadId);
		return this;
	}

	public MergeLeadsPage clickFirstLeadId() {
		click(leadId);
		switchToWindow(0);
		return new MergeLeadsPage();
	}
}
