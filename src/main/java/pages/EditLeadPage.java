package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class EditLeadPage extends ProjectMethods{

	public EditLeadPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="updateLeadForm_companyName")
	private WebElement companyName;
	public EditLeadPage editCompanyName(String data) {
		type(companyName, data);
		return new EditLeadPage();
	}
	
	@FindBy(xpath="//input[@value='Update']")
	private WebElement updateButton;
	public ViewLeadPage clickUpdateButton() {
		click(updateButton);
		return new ViewLeadPage();
	}
}
