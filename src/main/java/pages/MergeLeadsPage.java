package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MergeLeadsPage extends ProjectMethods{

	public MergeLeadsPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//table[@class='twoColumnForm']//img")
	private WebElement fromLeadIcon;
	public FromMergeFindLead clickFromLeadIcon() {
		click(fromLeadIcon);
		switchToWindow(1);
		return new FromMergeFindLead();
	}

	@FindBy(xpath = "(//table[@class='twoColumnForm']//img)[2]")
	private WebElement toLeadIcon;
	public ToMergeFindLead clickToLeadIcon() {
		click(toLeadIcon);
		switchToWindow(1);
		return new ToMergeFindLead();
	}
	@FindBy(linkText="Merge")
	private WebElement mergeButton;
	public ViewLeadPage clickMergeButton() {
		click(mergeButton);
		acceptAlert();
		return new ViewLeadPage();
	}
}