package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class LeadsMainPage extends ProjectMethods{
	
	public LeadsMainPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(linkText="Create Lead")
	private WebElement createLeadLink;
	@When ("I click create lead")
	public CreateLeadFormPage clickCreateLeadLink() {
	click(createLeadLink);
	return new CreateLeadFormPage();
	}
	
	@FindBy(linkText="Find Leads")
	private WebElement findLeads;
	public FindLeadPage clickFindLeadLink() {
	click(findLeads);
	return new FindLeadPage();
	}
	
	@FindBy(linkText="Merge Leads")
	private WebElement mergeLeads;
	public MergeLeadsPage clickMergeLeads() {
		click(mergeLeads);
		return new MergeLeadsPage();
	}
}
