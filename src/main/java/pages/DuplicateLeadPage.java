package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import wdMethods.ProjectMethods;

public class DuplicateLeadPage extends ProjectMethods {
	
	public DuplicateLeadPage () {
		PageFactory.initElements(driver, this);
	}
	
	public DuplicateLeadPage verifyDuplicateLeadTitle() {
	verifyTitle("Duplicate Lead | opentaps CRM");
	return this;
	}
	
	@FindBy(className = "smallSubmit")
	private WebElement createLeadButton;
	public ViewLeadPage clickCreateLeadButton() {
		click(createLeadButton);
		return new ViewLeadPage();
	}
}
