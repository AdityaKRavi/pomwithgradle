package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.When;
import wdMethods.ProjectMethods;

public class CreateLeadFormPage extends ProjectMethods{

	public CreateLeadFormPage() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(id="createLeadForm_companyName")
	private WebElement companyName;
	@When("I enter the comapny name as (.*)")
	public CreateLeadFormPage typeCompanyName(String data) {
		type(companyName, data);
		return this;
	}

	@FindBy(id="createLeadForm_firstName")
	private WebElement firstName;
	@When("enter the first name as (.*)")
	public CreateLeadFormPage typeFirstName(String data) {
		type(firstName, data);
		return this;
	}

	@FindBy(id="createLeadForm_lastName")
	private WebElement lastName;
	@When("enter the last name as (.*)")
	public CreateLeadFormPage typeLastName(String data) {
		type(lastName, data);
		return this;
	}

	@FindBy(id="createLeadForm_dataSourceId")
	private WebElement dataSource;
	public CreateLeadFormPage selectDataSource(String data) {
		selectDropDownUsingText(dataSource, data);
		return this;
	}

	@FindBy(id="createLeadForm_generalProfTitle")
	private WebElement title;
	public CreateLeadFormPage typeTitle(String data) {
		type(title, data);
		return this;
	}

	@FindBy(id="createLeadForm_annualRevenue")
	private WebElement annualRevenue;
	public CreateLeadFormPage typeAnnualRevenue(String data) {
		type(annualRevenue, data);
		return this;
	}

	@FindBy(id="createLeadForm_industryEnumId")
	private WebElement industryId;
	public CreateLeadFormPage selectIndustryId(String data) {
		selectDropDownUsingText(industryId, data);
		return this;
	}

	@FindBy(id="createLeadForm_ownershipEnumId")
	private WebElement ownershipId;
	public CreateLeadFormPage selectOwnershipId(String data) {
		selectDropDownUsingText(ownershipId, data);
		return this;
	}

	@FindBy(id="createLeadForm_sicCode")
	private WebElement sicCode;
	public CreateLeadFormPage typeSicCode(String data) {
		type(sicCode, data);
		return this;
	}

	@FindBy(id="createLeadForm_marketingCampaignId")
	private WebElement marketingCampaignId;
	public CreateLeadFormPage selectMarketingCampaignId(String data) {
		selectDropDownUsingText(marketingCampaignId, data);
		return this;
	}

	@FindBy(id="createLeadForm_departmentName")
	private WebElement departmentName;
	public CreateLeadFormPage typeDepartmentName(String data) {
		type(departmentName, data);
		return this;
	}

	@FindBy(id="createLeadForm_currencyUomId")
	private WebElement currency;
	public CreateLeadFormPage selectCurrency(String data) {
		selectDropDownUsingText(currency, data);
		return this;
	}

	@FindBy(id="createLeadForm_tickerSymbol")
	private WebElement tickerSymbol;
	public CreateLeadFormPage typeTickerSymbol(String data) {
		type(tickerSymbol, data);
		return this;
	}

	@FindBy(id="createLeadForm_primaryPhoneAreaCode")
	private WebElement phoneAreaCode;
	public CreateLeadFormPage typePhoneAreaCode(String data) {
		type(phoneAreaCode, data);
		return this;
	}

	@FindBy(id="createLeadForm_primaryPhoneNumber")
	private WebElement phoneNumber;
	public CreateLeadFormPage typePhoneNumber(String data) {
		type(phoneNumber, data);
		return this;
	}

	@FindBy(id="createLeadForm_primaryEmail")
	private WebElement emailAddress;
	public CreateLeadFormPage typeEmailAddress(String data) {
		type(emailAddress, data);
		return this;
	}

	@FindBy(id="createLeadForm_primaryWebUrl")
	private WebElement webUrl;
	public CreateLeadFormPage typeWebUrl(String data) {
		type(webUrl, data);
		return this;
	}

	@FindBy(id="createLeadForm_generalAddress1")
	private WebElement address1;
	public CreateLeadFormPage typeAddress1(String data) {
		type(address1, data);
		return this;
	}

	@FindBy(id="createLeadForm_generalAddress2")
	private WebElement address2;
	public CreateLeadFormPage typeAddress2(String data) {
		type(address2, data);
		return this;
	}

	@FindBy(id="createLeadForm_generalCity")
	private WebElement city;
	public CreateLeadFormPage typeCity(String data) {
		type(city, data);
		return this;
	}

	@FindBy(id="createLeadForm_generalCountryGeoId")
	private WebElement country;
	public CreateLeadFormPage selectCountry(String data) {
		selectDropDownUsingText(country, data);
		return this;
	}

	@FindBy(id="createLeadForm_generalStateProvinceGeoId")
	private WebElement state;
	public CreateLeadFormPage selectState(String data) {
		selectDropDownUsingText(state, data);
		return this;
	}

	@FindBy(id="createLeadForm_generalPostalCode")
	private WebElement zipCode;
	public CreateLeadFormPage typeZipCode(String data) {
		type(zipCode, data);
		return this;
	}	

	@FindBy(className="smallSubmit")
	private WebElement submit;
	@When ("I click on create lead button")
	public ViewLeadPage clickCreateLeadButton() {
		click(submit);
		return new ViewLeadPage();
	}
}
