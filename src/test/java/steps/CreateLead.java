//package steps;
//
//
//import java.util.concurrent.TimeUnit;
//
//import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.remote.RemoteWebDriver;
//
//import cucumber.api.java.en.And;
//import cucumber.api.java.en.Given;
//import cucumber.api.java.en.Then;
//import cucumber.api.java.en.When;
//
//public class CreateLead {
//	
//	RemoteWebDriver driver =null;
//	
//	@Given("The browser is already launched")
//	public void theBrowserIsAlreadyLaunched() {
//		if(driver!=null)
//		System.out.println("The driver is launched");
//	}
//	
//	@When ("Enter username (.*)")
//	public void typeUserName(String data) {
//		driver.findElementById("username").sendKeys(data);
//	}
//	
//	@And ("Enter password (.*)")
//	public void typePassword(String data) {
//		driver.findElementById("password").sendKeys(data);
//	}
//	
//	@When ("click on login button")
//	public void clickLogin() {
//		driver.findElementByClassName("decorativeSubmit").click();
//	}
//	
//	@Then ("verify login is success")
//	public void verifyLogin() {
//		System.out.println("Login verified...");
//	}
//
//	@Given("I am on home page")
//	public void i_am_on_home_page() {
//		System.out.println("Home Page verified");
//	}
//
//	@When("click on crmsfa")
//	public void click_on_crmsfa() {
//		driver.findElementByLinkText("CRM/SFA").click();
//		System.out.println("crmsfa clicked");
//	}
//
//	@Then("verify main page")
//	public void verify_main_page() {
//		System.out.println("Main Page verified");
//	}
//
//	@When("I click Leads tab")
//	public void i_click_Leads_tab() {
//		driver.findElementByLinkText("Leads").click();
//		System.out.println("lead tab clicked");
//	}
//	
//	@Then("enter main leads page")
//	public void enterMainLeadPage() {
//		System.out.println("I am on the main leads page");
//	}
//	
//	@When ("I click create lead")
//	public void clickCreateLead() {
//		driver.findElementByLinkText("Create Lead").click();
//		System.out.println("create lead clicked");
//	}
//	
//	@Then ("enter create lead page")
//	public void verifyCreateLeadPage() {
//		System.out.println("create lead page verified");
//	}
//	
//	@Given("I am on Create lead page")
//	public void iAmOnCreateLeadPage() {
//		System.out.println("I am on create lead page");
//	}
//
//	@When("I enter the comapny name as (.*)")
//	public void iEnterTheComapnyNameAs(String data) {
//		driver.findElementById("createLeadForm_companyName").sendKeys(data);
//	}
//
//	@When("enter the first name as (.*)")
//	public void enterTheFirstNameAs(String data) {
//		driver.findElementById("createLeadForm_firstName").sendKeys(data);;
//	}
//
//	@When("enter the last name as (.*)")
//	public void enterTheLastNameAs(String data) {
//		driver.findElementById("createLeadForm_lastName").sendKeys(data);;
//	}
//
//	@When ("I click on create lead button")
//	public void clickCreateLeadButton() {
//		driver.findElementByClassName("smallSubmit").click();	
//		System.out.println("create lead button clicked");
//	
//	}
//	@Then ("verify create lead is successfull")
//	public void verifyCreateLead() {
//		System.out.println("CreateLead verified");
//	}
//
//
//}
