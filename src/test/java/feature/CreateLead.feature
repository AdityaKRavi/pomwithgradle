Feature: Leaftap Automation

Scenario Outline: Creating the lead
Given I am on the Login Page
When Enter username <username>
And Enter password <password>
When click on login button
Then verify login is success
When click on crmsfa
When I click Leads tab
When I click create lead
When I enter the comapny name as <CompanyName>
And enter the first name as <FirstName>
And enter the last name as <LastName>
When I click on create lead button
Then verify create lead is successfull
Examples:

	|	username	 |	password	|	CompanyName	|	FirstName	|	LastName	|
	|DemoSalesManager|	crmsfa		|	HCL Tech	|	Ravi		|	Kumar		|
	|	DemoCSR  	 |	crmsfa		|	Google LLC	|	Mohan		|	Kumar		|
#####################################################################################
