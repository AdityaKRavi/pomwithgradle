package runner;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;

@CucumberOptions(features="src/test/java/feature/CreateLead.feature",
				glue= {"hooks","pages"},
				dryRun=false,
				snippets=SnippetType.CAMELCASE,
				monochrome=true,
				strict=false
//				plugin= {"pretty", "html:reports"}
				)
@RunWith(Cucumber.class)
public class RunTest {

}
