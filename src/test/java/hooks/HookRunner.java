package hooks;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import wdMethods.SeMethods;

public class HookRunner extends SeMethods {

	@Before
	public void beforeScenario(Scenario sc) {
		startResult();
		startTestModule(sc.getName(), sc.getId());
		test=startTestCase(sc.getId());
		test.assignCategory("smoke");
		test.assignAuthor("Aditya");
		startApp("chrome", "http://leaftaps.com/opentaps");
	}
	
	@After
	public void afterScenario(Scenario sc) {
		closeAllBrowsers();
		endResult();
	}
}
